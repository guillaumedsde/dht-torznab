## DHT-Torznab

## 🔮 Roadmap

- [x] Insert torrents into PGSQL DB from magneticod
- [x] Search for torrents by name
- [x] Functional minimal torznab API
- [ ] torznab API authentication
- [x] Implement a peer count updater
  - [bashkirtsevich-llc/aiobtdht](https://github.com/bashkirtsevich-llc)
  - [nitmir/btdht](https://github.com/nitmir/btdht)
- [ ] Search for torrents by name of its files
- [ ] Torrent classifier
- [ ] Fully featured torznab API
